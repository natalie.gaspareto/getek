package com.example.getek;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class telaCadastro extends AppCompatActivity implements View.OnClickListener {
    EditText txtEmail, txtPassword, txtConfirmPassword;
    CardView register, voltar;
    Toolbar toolbaar;

    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_cadastro);
        inicializarComponentes();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.sobre){
            finish();
            startActivity(new Intent(this, telaSobre.class));
        } if (id == R.id.idioma){
            finish();
            startActivity(new Intent( this, telaIdioma.class));
        }
        return true;
    }


    private void inicializarComponentes(){
        progressDialog = new ProgressDialog(this);
        txtEmail = (EditText) findViewById(R.id.input_email);
        txtPassword = (EditText) findViewById(R.id.input_password);
        voltar = (CardView) findViewById(R.id.btn_voltar_login_pelo_cadastro);
        txtConfirmPassword = (EditText) findViewById(R.id.input_confirm_password);
        register = (CardView) findViewById(R.id.btn_register);
        toolbaar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbaar);
        firebaseAuth = FirebaseAuth.getInstance();
        register.setOnClickListener(this);
        voltar.setOnClickListener(this);
    }


    private void registerUser(){
        String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();
        String confirm_password = txtConfirmPassword.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            //email is empty
            Toast.makeText(telaCadastro.this, "Insira um email", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(password)){
            //password is empty
            Toast.makeText(telaCadastro.this, "Insira uma senha", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(confirm_password)){
            Toast.makeText(telaCadastro.this, "Confirme sua senha", Toast.LENGTH_SHORT).show();
        }
        if(TextUtils.equals(password, confirm_password)){
            progressDialog.setMessage("Registrando usuário...");
            progressDialog.show();

            firebaseAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressDialog.dismiss();
                            if(task.isSuccessful()){
                                //user is succesfully registrated and logged in
                                Toast.makeText(telaCadastro.this, "Registrado com sucesso", Toast.LENGTH_SHORT).show();
                                txtEmail.setText("");
                                txtConfirmPassword.setText("");
                                txtPassword.setText("");
                            }else{
                                Toast.makeText(telaCadastro.this, "Não foi possível registrar, tente novamente", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
        else{
            Toast.makeText(telaCadastro.this, "Preencha corretamente os campos", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onClick(View view){
        if (view == register){
            registerUser();
        } if(view == voltar){
            startActivity(new Intent(telaCadastro.this, MainActivity.class));
        }
    }
}
