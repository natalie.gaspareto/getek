package com.example.getek;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    CardView btn_login, btn_voltar_login;
    TextView text_register, text_forgot_password;
    EditText username, password;
    CheckBox remember;
    Toolbar toolbaar;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firebaseAuth = FirebaseAuth.getInstance();

        inicialyzeComponents();

        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {// Verifica se o usuario está logado
            startActivity(new Intent(getApplicationContext(), telaPrincipal.class));
            Toast.makeText(MainActivity.this, "Seja bem vindo", Toast.LENGTH_SHORT).show();
        }
    }

    private void userLogin(){
        String email = username.getText().toString().trim();
        String passw = password.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            //email is empty
            Toast.makeText(this, "Preencha o email", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(passw)){
            //password is empty
            Toast.makeText(this, "Preencha a senha", Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.setMessage("Entrando no sistema...");
        progressDialog.show();

        firebaseAuth.signInWithEmailAndPassword(email, passw).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressDialog.dismiss();
                if (task.isSuccessful()) {
                    startActivity(new Intent(getApplicationContext(), telaPrincipal.class));
                } else {
                    Toast.makeText(MainActivity.this, "Usuário ou senha inválidos", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login,menu);
        return true;
    }
    @Override
    public void onClick(View view){
        if(view == btn_login){
            userLogin();
        } if (view == text_register){
            startActivity(new Intent(MainActivity.this, telaCadastro.class));
        } if(view == text_forgot_password){
            startActivity(new Intent(MainActivity.this, telaEsqueciSenha.class));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.sobre){
            finish();
            startActivity(new Intent(this, telaSobre.class));
        } if (id == R.id.idioma) {
            finish();
            startActivity(new Intent(this, telaIdioma.class));
        }
        return true;
    }

    private void inicialyzeComponents (){
        btn_login = (CardView) findViewById(R.id.btn_login);
        btn_voltar_login = (CardView) findViewById(R.id.btn_voltar_login);
        remember = (CheckBox) findViewById(R.id.btn_remember);
        text_register = (TextView) findViewById(R.id.text_register);
        text_forgot_password = (TextView) findViewById(R.id.text_forgot_password);
        username = (EditText) findViewById(R.id.input_user);
        password = (EditText) findViewById(R.id.input_password);
        toolbaar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbaar);
        progressDialog = new ProgressDialog(this);
        text_register.setOnClickListener(this);
        remember.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        text_forgot_password.setOnClickListener(this);
    }
}
