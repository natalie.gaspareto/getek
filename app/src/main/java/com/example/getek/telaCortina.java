package com.example.getek;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.google.firebase.auth.FirebaseAuth;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class telaCortina extends AppCompatActivity implements View.OnClickListener {
    CardView btn_salvar, btn_cancelar, btn_abrir, btn_fechar, btn_voltar;
    EditText txt_hour;
    Toolbar toolbaar;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_cortina);

        inicializarComponentes();

    }

    @Override
    public void onClick(View view) {
        if(view == btn_salvar){
            String hour = txt_hour.getText().toString().trim();
            if (TextUtils.isEmpty(hour)) {
                Toast.makeText(this, "Insira uma hora válida", Toast.LENGTH_SHORT).show();
            } else{
                Toast.makeText(this, "Configuração salva", Toast.LENGTH_SHORT).show();
            }
        }

        if(view == btn_cancelar){
            String hour = txt_hour.getText().toString().trim();
            if(TextUtils.isEmpty(hour)){
                Toast.makeText(this, "Não possui configuração registrada", Toast.LENGTH_SHORT).show();
            } else{
                Toast.makeText(this, "Configuração cancelada", Toast.LENGTH_SHORT).show();
                txt_hour.setText("");
            }
        }

        if(view == btn_abrir){
            Toast.makeText(this, "Abrindo cortina", Toast.LENGTH_SHORT).show();
            String url = "http://192.168.0.151:80/?abre";
            request(url);
        }

        if(view == btn_fechar){
            Toast.makeText(this, "Fechando cortina", Toast.LENGTH_SHORT).show();
            String url = "http://192.168.0.151:80/?fecha";
            request(url);

        }

        if(view == btn_voltar){
            startActivity(new Intent(this, telaPrincipal.class));
        }
    }

    protected void request(String url){
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    final String myResponse = response.body().string();

                    telaCortina.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.sair){
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }
        if(id == R.id.sobre){
            finish();
            startActivity(new Intent(this, telaSobre.class));
        }
        return true;
    }

    private void inicializarComponentes(){
        btn_salvar = (CardView) findViewById(R.id.btn_salvar);
        btn_cancelar = (CardView) findViewById(R.id.btn_cancelar);
        btn_abrir = (CardView) findViewById(R.id.btn_abrir);
        btn_fechar = (CardView) findViewById(R.id.btn_fechar);
        btn_voltar = (CardView) findViewById(R.id.btn_voltar_tela_cortina);
        txt_hour = (EditText) findViewById(R.id.text_hour);
        toolbaar = (Toolbar) findViewById(R.id.toolbar);

        btn_salvar.setOnClickListener(this);
        btn_cancelar.setOnClickListener(this);
        btn_abrir.setOnClickListener(this);
        btn_fechar.setOnClickListener(this);
        btn_voltar.setOnClickListener(this);
        setSupportActionBar(toolbaar);

    }
}


