package com.example.getek;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import java.util.Locale;


public class telaIdioma extends AppCompatActivity implements View.OnClickListener {
    ImageView btn_french, btn_portuguese, btn_english, btn_spanish;
    CardView btn_voltar;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_idioma);
        inicializarComponentes();
    }

    @Override
    public void onClick(View view){
        if(view == btn_voltar){
            startActivity(new Intent(telaIdioma.this, telaPrincipal.class));
        } if(view == btn_english){
            setLocale("en");
            recreate();
        } if(view == btn_spanish){
            setLocale("es");
            recreate();
        } if(view == btn_french){
            setLocale("fr");
            recreate();
        } if (view == btn_portuguese){
            setLocale("pt");
            recreate();
        }
    }

    private void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources()
                .getDisplayMetrics());
        SharedPreferences.Editor editor = getSharedPreferences("Setting",MODE_PRIVATE).edit();
        editor.putString("My_lang", lang);
        editor.apply();
    }

    public void loadLocale(){
        SharedPreferences prefs = getSharedPreferences("Setting", Activity.MODE_PRIVATE);
        String language = prefs.getString("My_lang","");
        setLocale(language);
    }

    private void inicializarComponentes(){
        btn_voltar = (CardView) findViewById(R.id.btn_voltar_idioma);
        btn_french = (ImageView) findViewById(R.id.img_french);
        btn_portuguese = (ImageView) findViewById(R.id.img_portuguese);
        btn_english = (ImageView) findViewById(R.id.img_english);
        btn_spanish = (ImageView) findViewById(R.id.img_spanish);
        btn_voltar.setOnClickListener(this);
        btn_english.setOnClickListener(this);
        btn_portuguese.setOnClickListener(this);
        btn_spanish.setOnClickListener(this);
        btn_french.setOnClickListener(this);
    }
}
