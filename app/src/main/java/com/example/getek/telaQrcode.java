package com.example.getek;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class telaQrcode extends AppCompatActivity implements View.OnClickListener{
    CardView btn_voltar, btn_scanear;
    Toolbar toolbaar;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_qrcode);
        inicializarComponentes();
    }


    @Override
    public void onClick(View view){
        if(view==btn_voltar){
            startActivity(new Intent(telaQrcode.this, telaPrincipal.class));
        }
        if (view == btn_scanear){
            IntentIntegrator integrator = new IntentIntegrator(this);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
            integrator.setPrompt("Camera Scan");
            integrator.setCameraId(0);
            integrator.initiateScan();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);

        if (result != null){
            if(result.getContents() != null){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage(result.getContents());
                alertDialogBuilder.setTitle("Informações");
                alertDialogBuilder.show();
            }else{
                alert("Scan cancelado!");
            }

        }else{
            super.onActivityResult(requestCode, resultCode, data);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.sair){
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }
        if(id == R.id.sobre){
            finish();
            startActivity(new Intent(this, telaSobre.class));
        }
        return true;
    }

    private void alert(String msg){
        Toast.makeText(getApplicationContext(), msg,Toast.LENGTH_SHORT).show();
    }
    private void inicializarComponentes(){
        btn_voltar = (CardView) findViewById(R.id.btn_voltar_tela_principal_por_qrcode);
        btn_scanear = (CardView) findViewById(R.id.btn_camera);
        btn_voltar.setOnClickListener(this);
        btn_scanear.setOnClickListener(this);
        toolbaar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbaar);

    }

}
