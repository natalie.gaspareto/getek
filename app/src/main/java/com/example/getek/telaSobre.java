package com.example.getek;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class telaSobre extends AppCompatActivity implements View.OnClickListener{
    CardView voltar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_sobre);

        voltar = (CardView) findViewById(R.id.btn_voltar_tela_principal);
        voltar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == voltar) {
            finish();
            startActivity(new Intent(this, telaPrincipal.class));
        }
    }
}
