package com.example.getek;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

public class telaPrincipal extends AppCompatActivity implements View.OnClickListener {
    CardView btn_qrCode, btn_fale_conosco;
    ListView lista_de_cortinas;
    private FirebaseAuth firebaseAuth;
    private Toolbar toolbaar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        inicializarComponentes();
    }

    private void inicializarComponentes(){
        firebaseAuth = FirebaseAuth.getInstance();
        btn_qrCode = (CardView) findViewById(R.id.btn_qr_code);
        btn_fale_conosco = (CardView) findViewById(R.id.btn_fale_conosco);
        lista_de_cortinas = (ListView) findViewById(R.id.lista_cortinas);
        toolbaar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbaar);

        if(firebaseAuth.getCurrentUser() == null){
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }

        btn_fale_conosco.setOnClickListener(this);
        btn_qrCode.setOnClickListener(this);

        ArrayList<String> cortinas = new ArrayList<>();

        cortinas.add("Cortina Sala");

        ArrayAdapter arrayAdapter = new ArrayAdapter(
                this,
                android.R.layout.simple_list_item_1,cortinas);

        lista_de_cortinas.setAdapter(arrayAdapter);

        lista_de_cortinas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent appInfo = new Intent(telaPrincipal.this, telaCortina.class);
                startActivity(appInfo);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.sair){
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }
        if(id == R.id.sobre){
            finish();
            startActivity(new Intent(this, telaSobre.class));
        }
        if(id == R.id.idioma){
            finish();
            startActivity(new Intent(this, telaIdioma.class));
        }
        return true;
    }

    @Override
    public void onClick(View view){
        if (view == btn_fale_conosco){
            startActivity(new Intent(this, telaFaleConosco.class));
        } if(view == btn_qrCode){
            startActivity(new Intent(this, telaQrcode.class));
        }
    }
}
