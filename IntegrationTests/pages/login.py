"""
Page Object do Login
"""
from pages.basepage import BasePage
import firebase_admin
from firebase_admin import auth
from firebase_admin import credentials
from selenium.common.exceptions import NoSuchElementException

class Login(BasePage):
    def __init__(self, driver):
        super(Login, self).__init__(driver)
        self.btns = {
            'cortina_title': 'com.example.getek:id/text_title',
            'fechar': 'com.example.getek:id/btn_voltar_login',
            'email': 'com.example.getek:id/input_user',
            'senha': 'com.example.getek:id/input_password',
            'acessar': 'com.example.getek:id/textView',
            'options': '//android.widget.ImageView[@content-desc="Mais opções"]',
            'sair': '/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView'
        }


    def inicialize_app_service(self):
        if (not len(firebase_admin._apps)):
            cred = credentials.Certificate('C:\\Users\\henri\\Downloads\\getek-google-service.json') 
            firebase_admin.initialize_app(cred)

    def delete_user(self, email):
        try:
            user = auth.get_user_by_email(email)
            auth.delete_user(user.uid)
        except:
            ...

    def verify_screen(self):
        try:
            self.driver.find_element_by_id(self.btns['cortina_title'])
            btn_close = self.driver.find_element_by_xpath(self.btns['options'])
            btn_close.click()
            btn_sair = self.driver.find_element_by_xpath(self.btns['sair'])
            btn_sair.click()
        except NoSuchElementException:
            ...

    def tests_login(self):
        field_email = self.driver.find_element_by_id(self.btns['email'])
        field_password = self.driver.find_element_by_id(self.btns['senha'])
        btn_acess = self.driver.find_element_by_id(self.btns['acessar'])
        field_email.send_keys('admin@getek.com')
        field_password.send_keys('123Mudar*')
        btn_acess.click()
