"""
Page Object do Registro
"""
from pages.basepage import BasePage

class Register(BasePage):
    def __init__(self, driver):
        super(Register, self).__init__(driver)
        self.fields = {
            'email': 'com.example.getek:id/input_email',
            'senha': 'com.example.getek:id/input_password',
            'confirmar senha': 'com.example.getek:id/input_confirm_password'
        }

    def input_field(self, field, value):
        field = self.driver.find_element_by_id(self.fields[field])
        field.send_keys(value)

    def assert_field(self, text):
        assert True == True