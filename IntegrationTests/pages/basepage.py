from time import sleep


class BasePage:
    def __init__(self, driver):
        self.driver = driver
        self.buttons = {
            'Cadastrar-se': 'com.example.getek:id/textView',
            'registro': 'com.example.getek:id/text_register',
            'cortina sala': 'android:id/text1',
            'fechar': 'com.example.getek:id/text_fechar',
            'abrir': 'com.example.getek:id/text_abrir',
            'configurar por hora': 'com.example.getek:id/text_hour',
            'salvar': 'com.example.getek:id/text_salvar',
            'cancelar': 'com.example.getek:id/text_cancelar'
        }

    def click_btn(self, btn):
        """
        Clica no botão

        Args:
            - btn: Botão que será pressionado
        """
        btn = self.driver.find_element_by_id(self.buttons[btn])
        btn.click()
    
    def configure_field(self, selector, input):
        input_selector = self.driver.find_element_by_id(self.buttons[selector])
        input_selector.send_keys(input)

    def assert_field_text(self, field):
        field_text = self.driver.find_element_by_id(
            self.buttons[field]).get_attribute("text")
        assert field_text == 'XX:XX'