"""
Hooks globais
"""
from caps import define_caps
from time import sleep
from pages.basepage import BasePage
from pages.login import Login


def before_all(context):
    context.target = context.config.userdata.get("target")
    context.name_device = context.config.userdata.get("name_device")
    context.name_platform = context.config.userdata.get("name_platform")
    context.platform_version = context.config.userdata.get("platform_version")


def before_feature(context, feature):
    context.driver = define_caps(
        target=context.target,
        device_name=context.name_device,
        platform_name=context.name_platform,
        platform_version=context.platform_version
    )
    page = Login(context.driver)
    page.inicialize_app_service()
    page.verify_screen()

def before_scenario(context, scenario):
    ...

def before_step(context, step):
    ...

def after_step(context, step):
    sleep(3)

def after_scenario(context, scenario):
    ...

def after_feature(context, feature):
    page = Login(context.driver)
    page.delete_user("teste2@getek.com")
    context.driver.quit()

def after_all(context):
    ...
