Funcionalidade: Testar a função da cortina

    Contexto: 
        Dado que faça o login com as credenciais de testes
        E clicar no botão "cortina sala"

    Cenario: Validar a funcionalidade básica da cortina
        Dado clicar no botão "abrir"
        Entao deverá ser exibido uma mensagem "Abrindo cortina"
        Dado clicar no botão "fechar"
        Entao deverá ser exibido uma mensagem "Fechando cortina"
        Dado que insira a hora "08:00" no campo "configurar por hora"
        Quando clicar no botão "salvar"
        Entao deverá ser exibido uma mensagem "Configuração salva"
        Quando clicar no botão "cancelar"
        Então deverá ser exibido uma mensagem "Configuração cancelada"
        E o campo "configurar por hora" deverá estar vazio
