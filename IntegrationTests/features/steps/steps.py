"""
Steps genéricos utilizados em mais de uma página
"""
from pages.basepage import BasePage
from pages.register import Register
from pages.login import Login
from behave import step


@step('que esteja na pagina de "{page_name}"')
def step_impl(context, page_name):
    page = BasePage(context.driver)
    page.click_btn(page_name)

@step('que insira "{value}" no campo "{field}"')
def insert_text(context, value, field):
    page = Register(context.driver)
    page.input_field(field, value)

@step('clicar no botão "{button}"')
def click_button(context, button):
    page = BasePage(context.driver)
    page.click_btn(button)

@step('deverá aparecer uma mensagem de progresso "{text}"')
def assert_progress_message(context, text):
    page = Register(context.driver)
    page.assert_field(text)

@step('deverá ser exibido uma mensagem "{text}"')
def assert_message(context, text):
    page = Register(context.driver)
    page.assert_field(text)

@step('que faça o login com as credenciais de testes')
def login_with_tests(context):
    page = Login(context.driver)
    page.tests_login()

@step('que insira a hora "{hour}" no campo "{field}"')
def configure_hour(context, hour, field):
    page = BasePage(context.driver)
    page.configure_field(field,hour)

@step('o campo "{field}" deverá estar vazio')
def assert_hour(context, field):
    page = BasePage(context.driver)
    