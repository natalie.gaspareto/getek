"""
Inicializar capabilities no emulador
"""

def desired_caps(device_name: str, platform_name: str, platform_version):
    """
    Inicializa as capabilities para um emulador
    """
    return {
        'deviceName': device_name,
        'platformName': platform_name,
        'appPackage': 'com.example.getek',
        'platformVersion': platform_version,
        'appActivity': '.MainActivity',
        'noReset': True
    }