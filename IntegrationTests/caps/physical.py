"""
Inicializar capabilities no device fisico
"""
def desired_caps(device_name, platform_name, platform_version):
    """
    Inicializa as capabilities para um device real
    """
    return {
        'deviceName': device_name,
        'platformName': platform_name,
        'appPackage': 'com.example.getek',
        'platformVersion': platform_version,
        'appActivity': '.MainActivity',
        'noReset': True
    }